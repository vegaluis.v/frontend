<?php
if (isset($_GET[id]) and $_GET[id] != '' and is_numeric($_GET[id])) {
    $datos_ver = peticion_get($_GET[id]);
    ?>
    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Ver Nota</legend>
            <input type="hidden" value="<?php echo "$datos_ver[id]"; ?>">
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Titulo</label>
                <div class="col-md-4">
                    <input id="textinput" type="text" placeholder="Titulo"
                           class="form-control input-md"
                           value="<?php echo "$datos_ver[title]"; ?>" disabled>
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="content">Contenido</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="content" disabled><?php echo "$datos_ver[content]"; ?></textarea>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="boton"></label>
                <div class="col-md-4">
                    <a href="./index.php" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Volver</a>
                    <a href="./index.php?sub=modificar&id=<?php echo $_GET[id]; ?>" class="btn btn-warning"><span
                                class="glyphicon glyphicon-pencil"></span> Modificar </a>
                </div>
            </div>

        </fieldset>
    </form>
    <?php
}
?>
