<?php
$data_tabla = peticion_get();
?>
<div class="container">
    <h2>Listado de Notas</h2>
    <a href="./index.php?sub=agregar" class="btn btn-info"> Nota Nueva </a>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>ID</th>
            <th>Titulo</th>
            <th>Contenido</th>
            <th>Ver</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data_tabla as $fila) {
            ?>
            <tr>
                <td><?php echo $fila[id]; ?></td>
                <td><?php echo $fila[title]; ?></td>
                <td><?php echo $fila[content]; ?></td>
                <td><a href="./index.php?sub=ver&id=<?php echo $fila[id]; ?>" class="btn btn-info"><span
                                class="glyphicon glyphicon-eye-open"></span> Ver</a></td>
                <td><a href="" class="btn btn-danger" onclick="fnEliminar(<?php echo $fila[id]; ?>)"><span
                                class="glyphicon glyphicon-trash"></span> Eliminar</a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
<script>

    function fnEliminar(id) {
        $.post("./ajax.php", {id_nota: id}, function (data) {
            setTimeout("location.href = 'index.php';", 0);
        })
    }
</script>
