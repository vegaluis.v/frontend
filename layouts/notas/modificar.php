<?php
if ($_POST and isset($_POST[boton]) and $_POST[boton] == 'modificar') {
    peticion_put($_POST[id], $_POST[title], $_POST[content]);
    redireccionar("./index.php?sub=ver&id=$_POST[id]");
}
if (isset($_GET[id]) and $_GET[id] != '' and is_numeric($_GET[id])) {
    $datos_ver = peticion_get($_GET[id]);

    ?>
    <form class="form-horizontal" method="post" action="">
        <fieldset>

            <!-- Form Name -->
            <legend>Modificar Nota</legend>
            <input type="hidden" name="id" value="<?php echo "$datos_ver[id]"; ?>">
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Titulo</label>
                <div class="col-md-4">
                    <input id="textinput" type="text" placeholder="Titulo" name="title"
                           class="form-control input-md"
                           value="<?php echo "$datos_ver[title]"; ?>">
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="content">Contenido</label>
                <div class="col-md-4">
                    <textarea class="form-control" name="content"
                              id="content"><?php echo "$datos_ver[content]"; ?></textarea>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="boton"></label>
                <div class="col-md-4">
                    <a href="./index.php?sub=ver&id=<?php echo $_GET[id]; ?>" class="btn btn-default"><span
                                class="glyphicon glyphicon-arrow-left"></span> Volver</a>
                    <button id="boton" name="boton" class="btn btn-success" value="modificar">Modificar</button>
                </div>
            </div>

        </fieldset>
    </form>
    <?php
}
?>
