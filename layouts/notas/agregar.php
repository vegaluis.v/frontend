<?php
if ($_POST and isset($_POST[boton]) and $_POST[boton] == 'agregar') {
    peticion_post($_POST[title], $_POST[content]);
    redireccionar("./index.php");
}
?>
<form class="form-horizontal" method="post" action="" autocomplete="off">
    <fieldset>

        <!-- Form Name -->
        <legend>Nueva Nota</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">Titulo</label>
            <div class="col-md-4">
                <input id="textinput" name="title" type="text" placeholder="Titulo" class="form-control input-md"
                       required="">

            </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="content">Contenido</label>
            <div class="col-md-4">
                <textarea class="form-control" id="content" name="content"></textarea>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="boton"></label>
            <div class="col-md-4">
                <button id="boton" name="boton" class="btn btn-success" value="agregar">Guardar</button>
            </div>
        </div>

    </fieldset>
</form>