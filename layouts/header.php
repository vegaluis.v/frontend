<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>_LDS</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>


    <!-- Bootrstap (responsive) -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> <!--Para poder usar el data table  -->
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css"> <!--  Para poder usar el select menu -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">

    <!-- Scripts -->
    <script type="text/javascript" language="javascript" src="js/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/bootstrap-select.js"></script>
</head>
<body>
<?php
require_once './layouts/navbar.php';
?>
<div class="container" style="margin-top:70px">

