<?php
function peticion_get($id = '')
{
//    $url = "quadminds-notes-test.getsandbox.com/notes";
    $url = "quadminds-notes-test.getsandbox.com/notes";
    if ($id != '') {
        $url .= "/$id";
    }
    $conexion = curl_init();
// --- Url
    curl_setopt($conexion, CURLOPT_URL, $url);
// --- Petición GET.
    curl_setopt($conexion, CURLOPT_HTTPGET, TRUE);
// --- Cabecera HTTP.
    curl_setopt($conexion, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// --- Para recibir respuesta de la conexión.
    curl_setopt($conexion, CURLOPT_RETURNTRANSFER, 1);
// --- Respuesta
    $respuesta = curl_exec($conexion);
    if ($respuesta === false) return false;
    curl_close($conexion);

    $respuesta = json_decode($respuesta, true);
    return $respuesta[data];
}

function peticion_post($title, $content)
{
    $url = "quadminds-notes-test.getsandbox.com/notes";
    $conexion = curl_init();
    $envio = json_encode(['title' => $title, 'content' => $content]); // --- Puede ser un xml, un json, etc.
    curl_setopt($conexion, CURLOPT_URL, $url);
// --- Datos que se van a enviar por POST.
    curl_setopt($conexion, CURLOPT_POSTFIELDS, $envio);
// --- Cabecera incluyendo la longitud de los datos de envio.
    curl_setopt($conexion, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($envio)));
// --- Petición POST.
    curl_setopt($conexion, CURLOPT_POST, 1);
// --- HTTPGET a false porque no se trata de una petición GET.
    curl_setopt($conexion, CURLOPT_HTTPGET, FALSE);
// -- HEADER a false.
    curl_setopt($conexion, CURLOPT_HEADER, FALSE);
// --- Respuesta.
    $respuesta = curl_exec($conexion);
    if ($respuesta === false) return false;
    curl_close($conexion);
    $respuesta = json_decode($respuesta, true);
    return $respuesta[data];
}

function peticion_put($id, $title, $content)
{
    $url = "quadminds-notes-test.getsandbox.com/notes/$id";
    $conexion = curl_init();
    $envio = json_encode(['title' => $title, 'content' => $content]); // --- Puede ser un xml, un json, etc.
    curl_setopt($conexion, CURLOPT_URL, $url);
// --- Datos que se van a enviar por PUT.
    curl_setopt($conexion, CURLOPT_POSTFIELDS, $envio);
// --- Cabecera incluyendo la longitud de los datos de envio.
    curl_setopt($conexion, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($envio)));
// --- Petición PUT.
    curl_setopt($conexion, CURLOPT_CUSTOMREQUEST, "PUT");
// --- HTTPGET a false porque no se trata de una petición GET.
    curl_setopt($conexion, CURLOPT_HTTPGET, FALSE);
// --- Respuesta.
    $respuesta = curl_exec($conexion);
    if ($respuesta === false) return false;
    curl_close($conexion);
//    $respuesta = json_decode($respuesta, true);
//    return $respuesta[data];
    return true;
}

function peticion_delete($id)
{
    $url = "quadminds-notes-test.getsandbox.com/notes/$id";
    $conexion = curl_init();
    curl_setopt($conexion, CURLOPT_URL, $url);
// --- Cabecera
    curl_setopt($conexion, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// --- Petición DELETE.
    curl_setopt($conexion, CURLOPT_CUSTOMREQUEST, "DELETE");
// --- HTTPGET a false porque no se trata de una petición GET.
    curl_setopt($conexion, CURLOPT_HTTPGET, FALSE);
// --- Respuesta.
    $respuesta = curl_exec($conexion);
    if ($respuesta === false) return false;
    curl_close($conexion);
    $respuesta = json_decode($respuesta, true);
    return $respuesta[data];

}

function redireccionar($direccion, $delay = '0')
{
    ?>
    <script type='text/JavaScript'>
        setTimeout("location.href = '<?php echo $direccion; ?>';",<?php echo $delay; ?>);
    </script>
    <?php
}