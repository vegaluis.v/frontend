<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <title>Titulo</title>
</head>
<body>
<div class="container">
    <h2>Listado de Notas</h2>
    <table class="table" id="tablaBs">
        <thead>
        <tr>
            <th data-field="id">ID</th>
            <th data-field="title">Titulo</th>
            <th data-field="content">Contenido</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
</body>
</html>
<script>
    const content = document.getElementById('content');
    fetch('https://quadminds-notes-test.getsandbox.com/notes')
        .then(respuesta => respuesta.json())
        .then(respuesta => {
            respuesta.data.forEach(e => {
                insFila(e);
            });
        });

    const insFila = e => {
        const fila = `<tr>
                        <td>${e.id}</td>
                        <td>${e.title}</td>
                        <td>${e.content}</td>
                    </tr>`;
        content.insertAdjacentHTML('beforeEnd', fila);
    };
</script>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>