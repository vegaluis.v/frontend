<?php
require_once "./core/base.php";
require_once "./layouts/header.php";

switch ($_GET["sub"]) {
    case 'agregar':
        require_once './layouts/notas/agregar.php';
        break;
    case 'ver': // root
        require_once './layouts/notas/ver.php';
        break;
    case 'modificar': // root
        require_once './layouts/notas/modificar.php';
        break;
    default:
        require_once './layouts/notas/listar.php';
        break;
} // terminamos con el switch

require_once "./layouts/footer.php";
?>